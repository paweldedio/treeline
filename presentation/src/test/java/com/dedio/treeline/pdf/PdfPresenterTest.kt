package com.dedio.treeline.pdf

import com.dedio.domain.interactor.pdf.LoadPdfUseCase
import com.dedio.domain.model.pdf.PdfRequest
import com.dedio.domain.model.pdf.PdfResponse
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Observable
import org.spekframework.spek2.Spek
import org.spekframework.spek2.style.specification.describe
import java.io.InputStream
import java.util.concurrent.TimeoutException

class PdfPresenterTest : Spek({

    val loadPdfUseCase by memoized { mock<LoadPdfUseCase>() }

    val pdfUrl = "http://google.com/test.pdf"
    val stream by memoized { mock<InputStream>() }
    val response by memoized { PdfResponse(stream) }
    val view by memoized { mock<PdfView>() }

    val subject by memoized { PdfPresenter(loadPdfUseCase) }

    fun prepareUseCase() {
        whenever(loadPdfUseCase.execute(any())).thenReturn(Observable.just(response))
    }

    beforeEachTest {
        prepareUseCase()

        subject.setArguments(pdfUrl)
        subject.attachViewWithLifecycle(view)
    }

    describe("create") {
        fun callMethod() {
            subject.create()
        }

        it("should request for correct pdf") {
            callMethod()

            verify(loadPdfUseCase).execute(PdfRequest(pdfUrl))
        }

        context("data load success") {
            beforeEachTest {
                callMethod()
            }

            it("should display pdf") {
                verify(view).renderPdf(stream)
            }
        }

        context("data load failure") {
            val message = "Error"

            beforeEachTest {
                whenever(loadPdfUseCase.execute(any())).thenReturn(Observable.error(TimeoutException(message)))
                callMethod()
            }

            it("should show error") {
                verify(view).showSnackbar(message)
            }
        }
    }
})