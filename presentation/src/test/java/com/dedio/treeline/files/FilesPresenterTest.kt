package com.dedio.treeline.files

import android.graphics.drawable.Drawable
import com.dedio.domain.interactor.files.GetSortedFilesUseCase
import com.dedio.domain.model.files.*
import com.dedio.treeline.R
import com.dedio.treeline.navigation.ActivitiesNavigationService
import com.dedio.treeline.util.DrawablesHelper
import com.nhaarman.mockitokotlin2.*
import io.reactivex.Observable
import org.junit.Assert.*
import org.spekframework.spek2.Spek
import org.spekframework.spek2.style.specification.describe
import org.threeten.bp.LocalDate
import java.util.concurrent.TimeoutException

class FilesPresenterTest : Spek({

    val getSortedFilesUseCase by memoized { mock<GetSortedFilesUseCase>() }
    val drawablesHelper by memoized { mock<DrawablesHelper>() }
    val activitiesNavigationService by memoized { mock<ActivitiesNavigationService>() }

    val file by memoized { File("test", "test", "test", LocalDate.of(2019, 5, 10)) }
    val filesList by memoized {
        val list = ArrayList<File>()
        list.add(file)
        list
    }
    val filesResponse by memoized { FilesResponse(filesList) }
    val drawable by memoized { mock<Drawable>() }
    val view by memoized { mock<FilesView>() }

    val subject by memoized { FilesPresenter(getSortedFilesUseCase, drawablesHelper, activitiesNavigationService) }

    fun prepareUseCase() {
        whenever(getSortedFilesUseCase.execute(any())).thenReturn(Observable.just(filesResponse))
    }

    fun prepareDrawableHelper() {
        whenever(drawablesHelper.getDrawable(any())).thenReturn(drawable)
    }

    beforeEachTest {
        prepareUseCase()
        prepareDrawableHelper()

        subject.attachViewWithLifecycle(view)
    }

    describe("create") {

        fun callMethod() {
            subject.create()
        }

        context("saved state is empty") {
            beforeEachTest {
                subject.attachViewWithLifecycle(view, null)
            }

            it("should call use case with default params") {
                callMethod()

                verify(getSortedFilesUseCase).execute(GetSortedFilesRequest(SortField.NAME, SortOrder.ASC))
            }

            context("files load success") {

                it("should show files") {
                    callMethod()

                    verify(view).showFiles(eq(filesResponse.files), any())
                }

                context("sort field is NAME") {
                    beforeEachTest {
                        subject.lastSortField = SortField.NAME
                    }

                    context("sort order is ASC") {
                        val correctDrawable = mock<Drawable>()

                        beforeEachTest {
                            whenever(drawablesHelper.getDrawable(R.drawable.arrow_up)).thenReturn(correctDrawable)

                            subject.lastSortOrder = SortOrder.ASC
                            callMethod()
                        }

                        it("should set correct indicator") {
                            verify(view).showNameSortIndicator(correctDrawable)
                        }
                    }

                    context("sort order is DESC") {
                        val correctDrawable = mock<Drawable>()

                        beforeEachTest {
                            whenever(drawablesHelper.getDrawable(R.drawable.arrow_down)).thenReturn(correctDrawable)

                            subject.lastSortOrder = SortOrder.DESC
                            callMethod()
                        }

                        it("should set correct indicator") {
                            verify(view).showNameSortIndicator(correctDrawable)
                        }
                    }
                }

                context("sort field is DESCRIPTION") {
                    beforeEachTest {
                        subject.lastSortField = SortField.DESCRIPTION
                    }

                    context("sort order is ASC") {
                        val correctDrawable = mock<Drawable>()

                        beforeEachTest {
                            whenever(drawablesHelper.getDrawable(R.drawable.arrow_up)).thenReturn(correctDrawable)

                            subject.lastSortOrder = SortOrder.ASC
                            callMethod()
                        }

                        it("should set correct indicator") {
                            verify(view).showDescriptionSortIndicator(correctDrawable)
                        }
                    }

                    context("sort order is DESC") {
                        val correctDrawable = mock<Drawable>()

                        beforeEachTest {
                            whenever(drawablesHelper.getDrawable(R.drawable.arrow_down)).thenReturn(correctDrawable)

                            subject.lastSortOrder = SortOrder.DESC
                            callMethod()
                        }

                        it("should set correct indicator") {
                            verify(view).showDescriptionSortIndicator(correctDrawable)
                        }
                    }
                }

                context("sort field is DATE") {
                    beforeEachTest {
                        subject.lastSortField = SortField.DATE
                    }

                    context("sort order is ASC") {
                        val correctDrawable = mock<Drawable>()

                        beforeEachTest {
                            whenever(drawablesHelper.getDrawable(R.drawable.arrow_up)).thenReturn(correctDrawable)

                            subject.lastSortOrder = SortOrder.ASC
                            callMethod()
                        }

                        it("should set correct indicator") {
                            verify(view).showDateSortIndicator(correctDrawable)
                        }
                    }

                    context("sort order is DESC") {
                        val correctDrawable = mock<Drawable>()

                        beforeEachTest {
                            whenever(drawablesHelper.getDrawable(R.drawable.arrow_down)).thenReturn(correctDrawable)

                            subject.lastSortOrder = SortOrder.DESC
                            callMethod()
                        }

                        it("should set correct indicator") {
                            verify(view).showDateSortIndicator(correctDrawable)
                        }
                    }
                }
            }

            context("files load failure") {
                val message = "Error"

                beforeEachTest {
                    whenever(getSortedFilesUseCase.execute(any())).thenReturn(Observable.error(TimeoutException(message)))
                }

                it("should show error") {
                    callMethod()

                    verify(view).showSnackbar(message)
                }
            }
        }

        context("saved state is not empty") {
            beforeEachTest {
                val state = FilesState(SortField.DATE, SortOrder.DESC)
                subject.attachViewWithLifecycle(view, state)

                callMethod()
            }

            it("should call use case with correct params") {
                verify(getSortedFilesUseCase).execute(GetSortedFilesRequest(SortField.DATE, SortOrder.DESC))
            }
        }
    }

    describe("onFileClick") {
        fun callMethod(file: File) {
            subject.onFileClick(file)
        }

        it("should open PdfActivity") {
            callMethod(file)

            verify(activitiesNavigationService).openPdfActivity(file.url)
        }
    }

    describe("onNameLabelClick") {
        fun callMethod() {
            subject.onNameLabelClick()
        }

        context("current sort field is NAME") {
            beforeEachTest {
                subject.lastSortField = SortField.NAME
                callMethod()
            }

            it("should load files with direction DESC") {
                verify(getSortedFilesUseCase).execute(GetSortedFilesRequest(SortField.NAME, SortOrder.DESC))
            }
        }

        context("current sort field is not NAME") {
            beforeEachTest {
                subject.lastSortField = SortField.DATE
                callMethod()
            }

            it("should load files with direction ASC") {
                verify(getSortedFilesUseCase).execute(GetSortedFilesRequest(SortField.NAME, SortOrder.ASC))
            }
        }
    }

    describe("onDescriptionLabelClick") {
        fun callMethod() {
            subject.onDescriptionLabelClick()
        }

        context("current sort field is DESCRIPTION") {
            beforeEachTest {
                subject.lastSortField = SortField.DESCRIPTION
                callMethod()
            }

            it("should load files with direction DESC") {
                verify(getSortedFilesUseCase).execute(GetSortedFilesRequest(SortField.DESCRIPTION, SortOrder.DESC))
            }
        }

        context("current sort field is not DESCRIPTION") {
            beforeEachTest {
                subject.lastSortField = SortField.DATE
                callMethod()
            }

            it("should load files with direction ASC") {
                verify(getSortedFilesUseCase).execute(GetSortedFilesRequest(SortField.DESCRIPTION, SortOrder.ASC))
            }
        }
    }

    describe("onDateLabelClick") {
        fun callMethod() {
            subject.onDateLabelClick()
        }

        context("current sort field is DATE") {
            beforeEachTest {
                subject.lastSortField = SortField.DATE
                callMethod()
            }

            it("should load files with direction DESC") {
                verify(getSortedFilesUseCase).execute(GetSortedFilesRequest(SortField.DATE, SortOrder.DESC))
            }
        }

        context("current sort field is not DATE") {
            beforeEachTest {
                subject.lastSortField = SortField.NAME
                callMethod()
            }

            it("should load files with direction ASC") {
                verify(getSortedFilesUseCase).execute(GetSortedFilesRequest(SortField.DATE, SortOrder.ASC))
            }
        }
    }

    describe("returnStateToSave") {
        fun callMethod() = subject.returnStateToSave()

        it("should save last selected data") {
            subject.lastSortField = SortField.DATE
            subject.lastSortOrder = SortOrder.DESC

            val response = callMethod()

            assertEquals(SortField.DATE, response.sortField)
            assertEquals(SortOrder.DESC, response.sortOrder)
        }
    }
})