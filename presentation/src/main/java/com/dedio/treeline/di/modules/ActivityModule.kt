package com.dedio.treeline.di.modules

import android.app.Activity
import com.dedio.treeline.base.BaseActivity
import com.dedio.treeline.di.scopes.ActivityScope
import dagger.Module
import dagger.Provides

@Module
class ActivityModule(private val activity: BaseActivity) {

    @Provides
    @ActivityScope
    internal fun provideBaseActivity(): BaseActivity {
        return activity
    }

    @Provides
    @ActivityScope
    internal fun providesActivity(activity: BaseActivity) : Activity {
        return activity
    }
}