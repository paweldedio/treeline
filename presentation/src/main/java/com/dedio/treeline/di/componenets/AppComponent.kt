package com.dedio.treeline.di.componenets

import android.content.Context
import com.dedio.treeline.di.modules.ActivityModule
import com.dedio.treeline.di.modules.AppModule
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class])
interface AppComponent {

    fun plus(module: ActivityModule): ActivityComponent
    fun context(): Context
}