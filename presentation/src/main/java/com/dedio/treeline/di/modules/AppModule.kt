package com.dedio.treeline.di.modules

import android.content.Context
import com.dedio.data.net.PublicRestApiProviderImpl
import com.dedio.data.repository.FilesRepositoryImpl
import com.dedio.data.repository.PdfRepositoryImpl
import com.dedio.data.util.FilesSortHelperImpl
import com.dedio.data.util.SchedulerProviderImpl
import com.dedio.domain.net.PublicRestApi
import com.dedio.domain.net.PublicRestApiProvider
import com.dedio.domain.repository.FilesRepository
import com.dedio.domain.repository.PdfRepository
import com.dedio.domain.util.FilesSortHelper
import com.dedio.domain.util.SchedulerProvider
import com.dedio.treeline.MainApplication
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule(private val context: MainApplication) {

    @Provides
    @Singleton
    fun provideContext(): Context = context.baseContext

    @Provides
    @Singleton
    fun provideSchedulerProvider(impl: SchedulerProviderImpl): SchedulerProvider = impl

    @Provides
    @Singleton
    fun provideFilesRepository(impl: FilesRepositoryImpl): FilesRepository = impl

    @Provides
    @Singleton
    fun provideFilesSortHelperImpl(impl: FilesSortHelperImpl): FilesSortHelper = impl

    @Provides
    @Singleton
    fun providesRestApiProvider(): PublicRestApiProvider {
        return PublicRestApiProviderImpl()
    }

    @Provides
    @Singleton
    fun providesRestApi(
            restApiProvider: PublicRestApiProvider): PublicRestApi = restApiProvider.provideRestApi()

    @Provides
    @Singleton
    fun providePdfRepository(impl: PdfRepositoryImpl): PdfRepository = impl
}