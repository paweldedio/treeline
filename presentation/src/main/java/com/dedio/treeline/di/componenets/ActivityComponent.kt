package com.dedio.treeline.di.componenets

import com.dedio.treeline.files.FilesActivity
import com.dedio.treeline.di.modules.ActivityModule
import com.dedio.treeline.di.scopes.ActivityScope
import com.dedio.treeline.pdf.PdfActivity
import dagger.Subcomponent

@ActivityScope
@Subcomponent(modules = [ActivityModule::class])
interface ActivityComponent {

    fun inject(activity: FilesActivity)

    fun inject(activity: PdfActivity)
}