package com.dedio.treeline.common.features

interface HasComponent<C> {

    fun getComponent(): C
}