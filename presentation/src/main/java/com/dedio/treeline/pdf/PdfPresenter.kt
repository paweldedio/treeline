package com.dedio.treeline.pdf

import com.dedio.domain.interactor.pdf.LoadPdfUseCase
import com.dedio.domain.model.pdf.PdfRequest
import com.dedio.domain.model.pdf.PdfResponse
import com.dedio.treeline.base.BasePresenter
import com.dedio.treeline.base.BaseState
import javax.inject.Inject

class PdfPresenter @Inject constructor(private val loadPdfUseCase: LoadPdfUseCase) :
        BasePresenter<PdfView, BaseState>() {

    private lateinit var pdfUrl: String

    fun setArguments(pdfUrl: String) {
        this.pdfUrl = pdfUrl
    }

    override fun create() {
        addDisposable(
                loadPdfUseCase.execute(PdfRequest(pdfUrl))
                        .compose(applyOverlaysToObservable())
                        .subscribe(this::handleLoadingSuccess, this::handleError)
        )
    }

    private fun handleLoadingSuccess(response: PdfResponse) {
        view?.renderPdf(response.stream)
    }
}