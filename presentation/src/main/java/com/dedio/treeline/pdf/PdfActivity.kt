package com.dedio.treeline.pdf

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.dedio.domain.util.PDF_URL_KEY
import com.dedio.treeline.R
import com.dedio.treeline.base.BaseActivity
import com.dedio.treeline.base.BasePresenter
import com.dedio.treeline.base.BaseState
import com.dedio.treeline.base.BaseView
import com.github.barteksc.pdfviewer.util.FitPolicy
import kotlinx.android.synthetic.main.activity_pdf.*
import java.io.InputStream
import javax.inject.Inject

class PdfActivity : BaseActivity(), PdfView {

    @Inject
    lateinit var presenter: PdfPresenter

    override fun <T : BasePresenter<BaseView, BaseState>> getPresenter() = presenter as T

    companion object {
        fun createIntent(pdfUrl: String, context: Context): Intent {
            val intent = Intent(context, PdfActivity::class.java)
            intent.putExtra(PDF_URL_KEY, pdfUrl)

            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pdf)

        passArgumentsToPresenter()
        presenter.attachViewWithLifecycle(this)
    }

    override fun makeInject() = activityComponent.inject(this)

    private fun passArgumentsToPresenter() {
        val extras = intent.extras!!
        val carRepairId = extras.getString(PDF_URL_KEY)!!
        presenter.setArguments(carRepairId)
    }

    override fun renderPdf(stream: InputStream) {
        pdf_viewer.fromStream(stream)
                .pageFitPolicy(FitPolicy.WIDTH)
                .pageSnap(true)
                .pageFling(true)
                .load()
    }
}
