package com.dedio.treeline.pdf

import com.dedio.treeline.base.BaseView
import java.io.InputStream

interface PdfView : BaseView {

    fun renderPdf(stream: InputStream)
}