package com.dedio.treeline

import android.app.Application
import android.content.Context
import androidx.appcompat.app.AppCompatDelegate
import com.dedio.treeline.base.BaseActivity
import com.dedio.treeline.di.componenets.ActivityComponent
import com.dedio.treeline.di.componenets.AppComponent
import com.dedio.treeline.di.componenets.DaggerAppComponent
import com.dedio.treeline.di.modules.ActivityModule
import com.dedio.treeline.di.modules.AppModule

class MainApplication: Application() {

    private val appComponent: AppComponent by lazy {
        generateAppComponent()
    }

    companion object {
        fun getApplication(context: Context): MainApplication {
            return context.applicationContext as MainApplication
        }
    }

    private fun generateAppComponent(): AppComponent {
        return DaggerAppComponent.builder().run {
            appModule(AppModule(this@MainApplication))
            build()

        }
    }

    fun createActivityComponent(activity: BaseActivity): ActivityComponent {
        return appComponent.plus(ActivityModule(activity))
    }

    override fun onCreate() {
        super.onCreate()
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)
    }
}