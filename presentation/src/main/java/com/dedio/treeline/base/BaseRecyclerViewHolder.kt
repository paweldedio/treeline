package com.dedio.treeline.base

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.dedio.domain.model.base.BaseResponse

abstract class BaseRecyclerViewHolder<in T : BaseResponse>(view: View) : RecyclerView.ViewHolder(view) {

    abstract fun init(item: T)
}