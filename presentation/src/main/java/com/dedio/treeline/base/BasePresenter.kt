package com.dedio.treeline.base

import android.os.Handler
import android.os.Looper
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import com.dedio.treeline.BuildConfig
import io.reactivex.ObservableTransformer
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

abstract class BasePresenter<View : BaseView, State : BaseState> : LifecycleObserver {

    protected var view: View? = null
    protected var savedState: State? = null

    private var compositeDisposable: CompositeDisposable? = CompositeDisposable()

    fun attachViewWithLifecycle(view: View, savedState: State? = null) {
        this.view = view
        this.savedState = savedState
        this.view?.lifecycle?.addObserver(this)
    }

    protected fun addDisposable(disposable: Disposable) {
        compositeDisposable?.add(disposable)
    }

    protected fun resetDisposables() {
        compositeDisposable?.dispose()
        compositeDisposable = null
        compositeDisposable = CompositeDisposable()
    }

    protected fun <T> applyOverlaysToObservable(): ObservableTransformer<T, T> {
        return ObservableTransformer {
            it.doOnSubscribe { tryShowLoading() }
                    .doOnComplete { tryHideLoading() }
                    .doOnError { tryHideLoading() }
        }
    }

    private fun tryShowLoading() {
        if (isViewAvailable()) {
            view?.showLoading()
        }
    }

    private fun tryHideLoading() {
        if (isViewAvailable()) {
            view?.hideLoading()
        }
    }

    protected fun handleError(throwable: Throwable) {
        if (BuildConfig.DEBUG) {
            throwable.printStackTrace()
        }

        view?.showSnackbar(throwable.localizedMessage)
    }

    protected fun openInUiThread(action: () -> Unit) {
        val handler = Handler(Looper.getMainLooper())
        handler.post(action)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    fun onStop() {
        stop()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    fun onDestroy() {
        destroy()
        detachView()
        resetDisposables()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    fun onPause() {
        pause()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    fun onResume() {
        resume()
    }


    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    fun onStart() {
        start()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    fun onCreate() {
        create()
    }

    open fun stop() {}

    open fun destroy() {}

    open fun pause() {}

    open fun resume() {}

    open fun start() {}

    open fun create() {}

    open fun detachView() {
        view = null
        view?.lifecycle?.removeObserver(this)

    }

    open fun isViewAvailable(): Boolean {
        if (view == null || view?.lifecycle == null) {
            return false
        }

        val state = view?.lifecycle?.currentState

        return state!!.isAtLeast(Lifecycle.State.CREATED) && state != Lifecycle.State.DESTROYED
    }

    open fun returnStateToSave(): State? = null

    open fun restoreState(state: State) {

    }
}