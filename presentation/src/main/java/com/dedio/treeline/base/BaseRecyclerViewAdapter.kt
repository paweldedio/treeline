package com.dedio.treeline.base

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.dedio.domain.model.base.BaseResponse
import java.util.ArrayList

abstract class BaseRecyclerViewAdapter<T : BaseResponse, VH : BaseRecyclerViewHolder<T>>(private val itemCallback: DiffUtil.ItemCallback<T>) :
        RecyclerView.Adapter<VH>() {

    open var items: List<T> = ArrayList()
        set(value) {
            val oldList = field
            field = value
            notifyChanges(oldList, field, itemCallback)
        }

    var onItemClickListener: ((item: T) -> Unit)? = null
    var onItemLongClickListener: ((item: T) -> Unit)? = null

    override fun getItemCount() = items.size


    override fun onBindViewHolder(holder: VH, position: Int) {
        initLayout(holder, items[position])
        holder.itemView.setOnClickListener {
            onItemClickListener?.invoke(items[position])
        }
        holder.itemView.setOnLongClickListener {
            onItemLongClickListener?.invoke(items[position])
            onItemLongClickListener != null
        }
    }

    open fun initLayout(holder: VH, item: T) {
        holder.init(item)
    }

    protected fun inflate(parent: ViewGroup, layoutResId: Int): View {
        return LayoutInflater.from(parent.context).inflate(layoutResId, parent, false)
    }

    private fun notifyChanges(oldList: List<T>, newList: List<T>,
                              itemCallback: DiffUtil.ItemCallback<T>) {
        val diff = DiffUtil.calculateDiff(object : DiffUtil.Callback() {

            override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
                return itemCallback.areItemsTheSame(oldList[oldItemPosition], newList[newItemPosition])
            }

            override fun getOldListSize(): Int {
                return oldList.size
            }

            override fun getNewListSize(): Int {
                return newList.size
            }

            override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
                return itemCallback.areContentsTheSame(oldList[oldItemPosition], newList[newItemPosition])
            }
        }, true)

        diff.dispatchUpdatesTo(this)
    }
}