package com.dedio.treeline.base

import android.view.View
import androidx.lifecycle.LifecycleOwner

interface BaseView : LifecycleOwner {

    fun <T: BasePresenter<BaseView, BaseState>> getPresenter(): T

    fun showError(error: String)

    fun showToast(text: String)

    fun showToast(text: String, length: Int)

    fun showSnackbar(message: String)

    fun showSnackbarSticky(message: String, text: CharSequence, clickListener: View.OnClickListener)

    fun showLoading()

    fun hideLoading()
}