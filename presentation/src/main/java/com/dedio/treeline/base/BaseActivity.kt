package com.dedio.treeline.base

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.annotation.MainThread
import androidx.appcompat.app.AppCompatActivity
import com.dedio.treeline.MainApplication
import com.dedio.treeline.common.features.HasComponent
import com.dedio.treeline.di.componenets.ActivityComponent
import com.google.android.material.snackbar.Snackbar

const val STATE_KEY = "state"

abstract class BaseActivity: AppCompatActivity(), HasComponent<ActivityComponent>, BaseView {

    protected lateinit var activityComponent: ActivityComponent

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        createActivityComponent()
        makeInject()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        saveState(outState)
    }

    private fun createActivityComponent() {
        activityComponent = MainApplication.getApplication(this).createActivityComponent(this)
    }

    abstract fun makeInject()

    override fun getComponent(): ActivityComponent = activityComponent

    open fun saveState(bundle: Bundle) {
        val state = getPresenter<BasePresenter<BaseView, BaseState>>().returnStateToSave()
        bundle.putSerializable(STATE_KEY, state)
    }

    open fun <T: BaseState> restoreState(bundle: Bundle?): T? {
        return bundle?.getSerializable(STATE_KEY) as T?
    }

    @MainThread
    override fun showSnackbar(message: String) {
        showSnackbar(message, Snackbar.LENGTH_LONG)
    }

    @MainThread
    override fun showSnackbarSticky(message: String, text: CharSequence, clickListener: View.OnClickListener) {
        showSnackbar(message, Snackbar.LENGTH_INDEFINITE, text, clickListener)
    }

    @MainThread
    private fun showSnackbar(message: String, duration: Int) {
        showSnackbar(message, duration, null, null)
    }

    @MainThread
    private fun showSnackbar(message: String, duration: Int, actionText: CharSequence?,
                             clickListener: View.OnClickListener?) {

        val root = findViewById<View>(android.R.id.content)

        val snackbar = Snackbar.make(root, message, duration)

        if (actionText != null) {
            snackbar.setAction(actionText) { v ->
                snackbar.dismiss()
                clickListener?.onClick(v)
            }
        }
        snackbar.show()
    }

    override fun showToast(text: String) = showToast(text, Toast.LENGTH_SHORT)

    override fun showToast(text: String, length: Int) = Toast.makeText(this, text, length).show()

    override fun showError(error: String) = showSnackbar(error)

    override fun showLoading() {
    }

    override fun hideLoading() {
    }
}