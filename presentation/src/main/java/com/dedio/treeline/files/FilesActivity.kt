package com.dedio.treeline.files

import android.graphics.drawable.Drawable
import android.os.Bundle
import com.dedio.domain.model.files.File
import com.dedio.treeline.R
import com.dedio.treeline.base.BaseActivity
import com.dedio.treeline.base.BasePresenter
import com.dedio.treeline.base.BaseState
import com.dedio.treeline.base.BaseView
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class FilesActivity : BaseActivity(), FilesView {

    @Inject
    lateinit var presenter: FilesPresenter

    @Inject
    lateinit var adapter: FilesAdapter

    override fun <T : BasePresenter<BaseView, BaseState>> getPresenter() = presenter as T

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val state = restoreState<FilesState>(savedInstanceState)
        setClickListeners()

        presenter.attachViewWithLifecycle(this, state)
    }

    private fun setClickListeners() {
        files_name_label.setOnClickListener { presenter.onNameLabelClick() }
        files_description_label.setOnClickListener { presenter.onDescriptionLabelClick() }
        files_date_label.setOnClickListener { presenter.onDateLabelClick() }
    }

    override fun makeInject() = activityComponent.inject(this)

    override fun showFiles(list: List<File>, itemClickListener: (File) -> Unit) {
        adapter.onItemClickListener = itemClickListener
        adapter.items = list

        if (files_recycler.adapter == null) {
            files_recycler.adapter = adapter
        }
    }

    override fun clearSortIndicators() {
        files_name_label.setCompoundDrawables(null, null, null, null)
        files_description_label.setCompoundDrawables(null, null, null, null)
        files_date_label.setCompoundDrawables(null, null, null, null)
    }

    override fun showNameSortIndicator(drawable: Drawable?) {
        files_name_label.setCompoundDrawablesWithIntrinsicBounds(null, null, null, drawable)
    }

    override fun showDescriptionSortIndicator(drawable: Drawable?) {
        files_description_label.setCompoundDrawablesWithIntrinsicBounds(null, null, null, drawable)
    }

    override fun showDateSortIndicator(drawable: Drawable?) {
        files_date_label.setCompoundDrawablesWithIntrinsicBounds(null, null, null, drawable)
    }
}
