package com.dedio.treeline.files

import androidx.recyclerview.widget.DiffUtil
import com.dedio.domain.model.files.File
import javax.inject.Inject

class FileItemDiffCallback @Inject constructor(): DiffUtil.ItemCallback<File>() {

    override fun areItemsTheSame(oldItem: File, newItem: File) = oldItem.name == newItem.name

    override fun areContentsTheSame(oldItem: File, newItem: File) = oldItem == newItem
}