package com.dedio.treeline.files

import com.dedio.domain.model.files.SortField
import com.dedio.domain.model.files.SortOrder
import com.dedio.treeline.base.BaseState

data class FilesState(val sortField: SortField, val sortOrder: SortOrder): BaseState()