package com.dedio.treeline.files

import android.view.View
import com.dedio.domain.model.files.File
import com.dedio.treeline.base.BaseRecyclerViewHolder
import kotlinx.android.synthetic.main.cell_file.view.*

class FilesViewHolder(view: View): BaseRecyclerViewHolder<File>(view) {

    override fun init(item: File) {
        with(itemView) {
            cell_file_name.text = item.name
            cell_file_description.text = item.description
            cell_file_date.text = item.date.toString()
        }
    }
}