package com.dedio.treeline.files

import android.view.ViewGroup
import com.dedio.domain.model.files.File
import com.dedio.treeline.R
import com.dedio.treeline.base.BaseRecyclerViewAdapter
import javax.inject.Inject

class FilesAdapter @Inject constructor(diffCallback: FileItemDiffCallback): BaseRecyclerViewAdapter<File, FilesViewHolder>(diffCallback) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FilesViewHolder {
        val itemView = inflate(parent, R.layout.cell_file)
        return FilesViewHolder(itemView)
    }
}