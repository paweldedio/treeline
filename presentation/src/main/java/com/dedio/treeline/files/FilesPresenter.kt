package com.dedio.treeline.files

import android.graphics.drawable.Drawable
import com.dedio.domain.interactor.files.GetSortedFilesUseCase
import com.dedio.domain.model.files.*
import com.dedio.treeline.R
import com.dedio.treeline.base.BasePresenter
import com.dedio.treeline.navigation.ActivitiesNavigationService
import com.dedio.treeline.util.DrawablesHelper
import javax.inject.Inject

class FilesPresenter @Inject constructor(private val getSortedFilesUseCase: GetSortedFilesUseCase,
                                         private val drawablesHelper: DrawablesHelper,
                                         private val actvitiesNavigationService: ActivitiesNavigationService) :
        BasePresenter<FilesView, FilesState>() {

    internal var lastSortField = SortField.NAME
    internal var lastSortOrder = SortOrder.ASC

    override fun create() {
        lastSortField = savedState?.sortField ?: lastSortField
        lastSortOrder = savedState?.sortOrder ?: lastSortOrder
        savedState = null

        loadFiles(lastSortField, lastSortOrder)
    }

    private fun loadFiles(sortField: SortField, sortOrder: SortOrder) {
        resetDisposables()
        addDisposable(
                getSortedFilesUseCase
                        .execute(GetSortedFilesRequest(sortField, sortOrder))
                        .compose(applyOverlaysToObservable())
                        .subscribe({ handleSortedFiles(sortField, sortOrder, it) },
                                this::handleError)
        )
    }

    private fun handleSortedFiles(sortField: SortField, sortOrder: SortOrder,
                                  response: FilesResponse) {
        lastSortField = sortField
        lastSortOrder = sortOrder

        view?.showFiles(response.files, this::onFileClick)
        setSortingIndicator(sortField, sortOrder)
    }

    private fun setSortingIndicator(sortField: SortField, sortOrder: SortOrder) {
        val drawable = getDrawableForOrder(sortOrder)
        view?.clearSortIndicators()

        when (sortField) {
            SortField.NAME -> view?.showNameSortIndicator(drawable)
            SortField.DESCRIPTION -> view?.showDescriptionSortIndicator(drawable)
            SortField.DATE -> view?.showDateSortIndicator(drawable)
        }
    }

    private fun getDrawableForOrder(sortOrder: SortOrder): Drawable? {
        val drawableRes =  when (sortOrder) {
            SortOrder.ASC -> R.drawable.arrow_up
            SortOrder.DESC -> R.drawable.arrow_down
        }

        return drawablesHelper.getDrawable(drawableRes)
    }

    fun onFileClick(file: File) {
        actvitiesNavigationService.openPdfActivity(file.url)
    }

    fun onNameLabelClick() {
        val order = identifySortOrder(SortField.NAME)

        loadFiles(SortField.NAME, order)
    }

    fun onDescriptionLabelClick() {
        val order = identifySortOrder(SortField.DESCRIPTION)

        loadFiles(SortField.DESCRIPTION, order)
    }

    fun onDateLabelClick() {
        val order = identifySortOrder(SortField.DATE)

        loadFiles(SortField.DATE, order)
    }

    private fun identifySortOrder(sortField: SortField): SortOrder {
        return if(lastSortField == sortField) {
            lastSortOrder.negation()
        } else {
            SortOrder.ASC
        }
    }

    override fun returnStateToSave() = FilesState(lastSortField, lastSortOrder)
}