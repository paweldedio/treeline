package com.dedio.treeline.files

import android.graphics.drawable.Drawable
import com.dedio.domain.model.files.File
import com.dedio.treeline.base.BaseView

interface FilesView : BaseView {

    fun showFiles(list: List<File>, itemClickListener: (File) -> Unit)

    fun clearSortIndicators()

    fun showNameSortIndicator(drawable: Drawable?)

    fun showDescriptionSortIndicator(drawable: Drawable?)

    fun showDateSortIndicator(drawable: Drawable?)
}