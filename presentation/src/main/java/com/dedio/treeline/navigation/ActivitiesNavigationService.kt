package com.dedio.treeline.navigation

import com.dedio.treeline.base.BaseActivity
import com.dedio.treeline.pdf.PdfActivity
import javax.inject.Inject

class ActivitiesNavigationService @Inject constructor(private val activity: BaseActivity) {

    fun openPdfActivity(pdfUrl: String) {
        val intent = PdfActivity.createIntent(pdfUrl, activity)
        activity.startActivity(intent)
    }
}