# Overview
This app is solution for Treeline Interview Task

#Task description
Create a basic single view android application that provides sortable list of files with created date, filetype (icon), description and filename. The app should allow the ability to view the PDF and exit to return to the pdf list. For simplicity the PDF's will be hosted on an S3 bucket and the metadata will be provided. We are looking for clean code, easy to read and uses a software design pattern suitable for this task.


Please create a Read Me file describing what you did (technologies, libraries, architecture, process,  etc.) and why you chose to do it that way.

# Architecture
This app is written using Clean Architecture mixed with Model View Presenter.

We have 3 basic modules: 

* `domain` is plain java module (without android) - this module is responsible for defining use cases, data models and some interfaces
* `data` is android module - this module is responsible for implementing interfaces defined in `domain` module
* `presentation` is main android module - this module is responsible for defining  User Interface and interacting with other modules


# Running
In order to run this app in Android Studio, you need to create run configuration using `presentation` module.

# Usage
App is very simple. If you want to change sorting, you need to tap on column header (one of `Name`, `Description`, `Date`). If you want to open file, you need to just tap on list item

# Testing
Unit tests was written using [Spek Framework](https://spekframework.org). If you want to run them, I recommend [Spek Framework Plugin](https://plugins.jetbrains.com/plugin/10915-spek-framework), see [instruction](https://spekframework.org/running/)

# Used libraries and tools
* [Lifecycle Aware Components](https://developer.android.com/topic/libraries/architecture/lifecycle) - This library helps eliminate boilerplate related to android lifecycle. You can see usage of this library in `BasePresenter` class
* [RxJava 2](https://github.com/ReactiveX/RxJava) - Library used for reactive programming
* [Dagger 2](https://github.com/google/dagger) - Powerful dependency injection library
* [Retrofit](https://github.com/square/retrofit) - HTTP client
* [Android Pdf viewer](https://github.com/barteksc/AndroidPdfViewer) - Pdf renderer for android. Android has native renderer, but it required minimum API 21. This library is very popular and I have experience with it.
* [Spek Framework](https://spekframework.org) - Amazing tests framework for kotlin. Please take a look how beautiful these tests are <3

# Possible improvements
I was told this app should not be one billion dollar app, so sometimes I skipped little important things. In next release I would improve:

* Dynamic icon based on file type. For now I just hardcoded icon in xml `cell_file` because we have only one type. In next release I would add new mapper to `GetSortedFilesUseCase` which will return new object `ReadableFile` with correct icon res id based on file type.
* Better date formatting. For now I just used `LocalDate.toString()` method. In my other apps I have `DateFormatter` class which is responsible for this. I would put this to mapper that I've mentioned in previous point.
* Loading overlays. For now methods responsible for this are empty: `BaseActivity.showLoading()` and `BaseActivity.hideLoading()`
* Cache. For now every time when you change sorting, new portion of data is collecting from `FilesRepository`. Usually I implement some memory cache, especially when data comes from API.
* Pdf files downloading. This point is similar to previous. I would implement logic for downloading pdf files, and storing their in phone memory. Now every time when you want to open file `InputStream` is saved only in RAM and passed directly to PDF renderer
* Text copying. It would enable text copying in opened PDF document.
* Better error handling. For now I just show `Snackbar` when there is some exception.


