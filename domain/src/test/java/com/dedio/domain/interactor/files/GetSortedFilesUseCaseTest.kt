package com.dedio.domain.interactor.files

import com.dedio.domain.model.files.*
import com.dedio.domain.util.FilesSortHelper
import com.dedio.domain.util.SchedulerProvider
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import org.spekframework.spek2.Spek
import org.spekframework.spek2.style.specification.describe
import java.util.*

class GetSortedFilesUseCaseTest : Spek({

    val schedulerProvider by memoized { mock<SchedulerProvider>() }
    val getFilesUseCase by memoized { mock<GetFilesUseCase>() }
    val filesSortHelper by memoized { mock<FilesSortHelper>() }
    val comparator by memoized { compareBy<File> { it.name } }

    val filesResponse by memoized { FilesResponse(ArrayList()) }

    val subject by memoized {
        GetSortedFilesUseCase(schedulerProvider, getFilesUseCase, filesSortHelper)
    }

    fun prepareUseCaseResponse() {
        whenever(getFilesUseCase.execute(any())).thenReturn(Observable.just(filesResponse))
    }

    fun prepareComparator() {
        whenever(filesSortHelper.getFilesComparator(any(), any())).thenReturn(comparator)
    }

    fun prepareSchedulers() {
        whenever(schedulerProvider.provideIOScheduler()).thenReturn(Schedulers.trampoline())
        whenever(schedulerProvider.provideComputationScheduler()).thenReturn(
                Schedulers.trampoline())
    }

    beforeEachTest {
        prepareUseCaseResponse()
        prepareComparator()
        prepareSchedulers()
    }

    describe("createUseCaseObservable") {

        fun callMethod(
                param: GetSortedFilesRequest = GetSortedFilesRequest()): Observable<FilesResponse> {
            return subject.createUseCaseObservable(param)
        }

        it("should get files from another use case") {
            callMethod()

            verify(getFilesUseCase).execute(any())
        }

        it("should use correct files comparator") {
            val sortField = SortField.DATE
            val sortOrder = SortOrder.DESC

            callMethod(GetSortedFilesRequest(sortField, sortOrder)).test()

            verify(filesSortHelper).getFilesComparator(sortField, sortOrder)
        }
    }
})