package com.dedio.domain.net

import io.reactivex.Observable
import okhttp3.ResponseBody
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Streaming
import retrofit2.http.Url

interface PublicRestApi {

    @GET
    @Streaming
    fun loadPdf(@Url url: String): Observable<Response<ResponseBody>>
}