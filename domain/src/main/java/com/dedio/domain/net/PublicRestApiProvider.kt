package com.dedio.domain.net

interface PublicRestApiProvider {

    fun provideRestApi(): PublicRestApi
}