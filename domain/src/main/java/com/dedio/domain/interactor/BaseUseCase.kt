package com.dedio.domain.interactor

import com.dedio.domain.model.base.BaseRequest
import com.dedio.domain.model.base.BaseResponse
import com.dedio.domain.util.SchedulerProvider
import io.reactivex.Observable
import io.reactivex.Scheduler

abstract class BaseUseCase<Request : BaseRequest, Response : BaseResponse> (private val schedulerProvider: SchedulerProvider) {

    abstract fun createUseCaseObservable(param: Request): Observable<Response>

    fun execute(param: Request): Observable<Response> {
        val subscribeOnScheduler = provideSubscribeOnScheduler(schedulerProvider)
        val observeOnScheduler = provideObserveOnScheduler(schedulerProvider)
        var observable = createUseCaseObservable(param)

        if(subscribeOnScheduler != null) {
            observable = observable.subscribeOn(subscribeOnScheduler)
        }

        if(observeOnScheduler != null) {
            observable = observable.observeOn(observeOnScheduler)
        }

        return observable
    }

    protected open fun provideSubscribeOnScheduler(provider: SchedulerProvider): Scheduler? {
        return provider.provideIOScheduler()
    }

    protected open fun provideObserveOnScheduler(provider: SchedulerProvider): Scheduler? {
        return provider.provideMainThreadScheduler()
    }

    protected fun <X> toColdObservable(x: X): Observable<X> {
        return Observable.defer { Observable.just(x) }
    }
}