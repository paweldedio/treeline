package com.dedio.domain.interactor.files

import com.dedio.domain.interactor.BaseUseCase
import com.dedio.domain.model.base.BaseRequest
import com.dedio.domain.model.files.FilesResponse
import com.dedio.domain.repository.FilesRepository
import com.dedio.domain.util.SchedulerProvider
import io.reactivex.Observable
import javax.inject.Inject

class GetFilesUseCase @Inject constructor(schedulerProvider: SchedulerProvider,
                                          private val repository: FilesRepository) :
        BaseUseCase<BaseRequest, FilesResponse>(schedulerProvider) {

    override fun createUseCaseObservable(param: BaseRequest): Observable<FilesResponse> {
        return repository.getFiles()
    }
}