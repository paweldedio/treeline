package com.dedio.domain.interactor.pdf

import com.dedio.domain.interactor.BaseUseCase
import com.dedio.domain.model.pdf.PdfRequest
import com.dedio.domain.model.pdf.PdfResponse
import com.dedio.domain.repository.PdfRepository
import com.dedio.domain.util.SchedulerProvider
import io.reactivex.Observable
import io.reactivex.Scheduler
import javax.inject.Inject

class LoadPdfUseCase @Inject constructor(private val schedulerProvider: SchedulerProvider,
                                         private val repository: PdfRepository) :
        BaseUseCase<PdfRequest, PdfResponse>(schedulerProvider) {

    override fun createUseCaseObservable(param: PdfRequest): Observable<PdfResponse> {
        return repository.getPdfFile(param.url)
                .subscribeOn(schedulerProvider.provideIOScheduler())
                .observeOn(schedulerProvider.provideIOScheduler())
                .map { PdfResponse(it.body()!!.byteStream()) }
    }

    override fun provideSubscribeOnScheduler(provider: SchedulerProvider): Scheduler? = null
}