package com.dedio.domain.interactor.files

import com.dedio.domain.interactor.BaseUseCase
import com.dedio.domain.model.base.EmptyRequest
import com.dedio.domain.model.files.FilesResponse
import com.dedio.domain.model.files.GetSortedFilesRequest
import com.dedio.domain.util.FilesSortHelper
import com.dedio.domain.util.SchedulerProvider
import io.reactivex.Observable
import io.reactivex.Scheduler
import javax.inject.Inject

class GetSortedFilesUseCase @Inject constructor(private val schedulerProvider: SchedulerProvider,
                                                private val getFilesUseCase: GetFilesUseCase,
                                                private val filesSortHelper: FilesSortHelper) :
        BaseUseCase<GetSortedFilesRequest, FilesResponse>(schedulerProvider) {

    override fun createUseCaseObservable(param: GetSortedFilesRequest): Observable<FilesResponse> {
        return getFilesUseCase.execute(EmptyRequest())
                .subscribeOn(schedulerProvider.provideIOScheduler())
                .observeOn(schedulerProvider.provideComputationScheduler())
                .map { sortFiles(param, it) }
    }

    private fun sortFiles(param: GetSortedFilesRequest, filesResponse: FilesResponse): FilesResponse {
        filesResponse.files.sortWith(
                filesSortHelper.getFilesComparator(param.sortField, param.sortOrder))

        return filesResponse
    }

    override fun provideSubscribeOnScheduler(provider: SchedulerProvider): Scheduler? = null
}