package com.dedio.domain.repository

import com.dedio.domain.model.files.FilesResponse
import io.reactivex.Observable

interface FilesRepository {

    fun getFiles(): Observable<FilesResponse>
}