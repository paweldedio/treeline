package com.dedio.domain.repository

import io.reactivex.Observable
import okhttp3.ResponseBody
import retrofit2.Response

interface PdfRepository {

    fun getPdfFile(url: String): Observable<Response<ResponseBody>>
}