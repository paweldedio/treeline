package com.dedio.domain.model.files

import com.dedio.domain.model.base.BaseResponse

data class FilesResponse(var files: MutableList<File>): BaseResponse()