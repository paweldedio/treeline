package com.dedio.domain.model.pdf

import com.dedio.domain.model.base.BaseRequest

data class PdfRequest(var url: String): BaseRequest()