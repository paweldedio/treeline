package com.dedio.domain.model.files

import com.dedio.domain.model.base.BaseResponse
import org.threeten.bp.LocalDate

data class File(var name: String, var description: String, var url: String, var date: LocalDate) : BaseResponse()