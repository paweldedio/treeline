package com.dedio.domain.model.pdf

import com.dedio.domain.model.base.BaseResponse
import java.io.InputStream

data class PdfResponse(var stream: InputStream): BaseResponse()