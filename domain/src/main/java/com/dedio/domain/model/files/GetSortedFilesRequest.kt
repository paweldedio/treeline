package com.dedio.domain.model.files

import com.dedio.domain.model.base.BaseRequest

data class GetSortedFilesRequest(var sortField: SortField = SortField.NAME,
                                 var sortOrder: SortOrder = SortOrder.ASC) : BaseRequest()

enum class SortField {
    NAME, DESCRIPTION, DATE
}

enum class SortOrder {
    ASC, DESC;

    fun negation(): SortOrder {
        return if(this == ASC) {
            DESC
        } else {
            ASC
        }
    }
}