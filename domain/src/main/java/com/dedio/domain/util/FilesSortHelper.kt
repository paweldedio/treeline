package com.dedio.domain.util

import com.dedio.domain.model.files.File
import com.dedio.domain.model.files.SortField
import com.dedio.domain.model.files.SortOrder

interface FilesSortHelper {

    fun getFilesComparator(sortField: SortField, sortOrder: SortOrder): Comparator<File>
}