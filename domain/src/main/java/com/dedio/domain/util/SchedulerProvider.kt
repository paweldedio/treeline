package com.dedio.domain.util

import io.reactivex.Scheduler

interface SchedulerProvider {

    fun provideIOScheduler(): Scheduler

    fun provideComputationScheduler(): Scheduler

    fun provideMainThreadScheduler(): Scheduler
}