package com.dedio.domain.util

const val PDF_URL_KEY = "PDF_URL"

const val API_TIMEOUT = 30.toLong()