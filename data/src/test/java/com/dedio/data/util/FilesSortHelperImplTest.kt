package com.dedio.data.util

import com.dedio.domain.model.files.File
import com.dedio.domain.model.files.SortField
import com.dedio.domain.model.files.SortOrder
import org.junit.Assert.*
import org.spekframework.spek2.Spek
import org.spekframework.spek2.style.specification.describe
import org.threeten.bp.LocalDate

class FilesSortHelperImplTest : Spek({

    val filesList by memoized {
        val list = ArrayList<File>()
        list.add(File("bbb", "bbb", "bbb", LocalDate.of(2019, 5, 10)))
        list.add(File("ddd", "ddd", "ddd", LocalDate.of(2019, 5, 20)))
        list.add(File("aaa", "aaa", "aaa", LocalDate.of(2019, 5, 5)))
        list.add(File("ccc", "ccc", "ccc", LocalDate.of(2019, 5, 15)))
        list
    }

    val subject by memoized { FilesSortHelperImpl() }

    describe("getFilesComparator") {

        fun callMethod(sortField: SortField, sortOrder: SortOrder) = subject.getFilesComparator(
                sortField, sortOrder)

        context("Sort field is NAME") {
            val sortField = SortField.NAME

            context("Sort order is ASC") {
                val sortOrder = SortOrder.ASC
                var response: List<File>? = null

                beforeEachTest {
                    response = filesList.sortedWith(callMethod(sortField, sortOrder))
                }

                it("should be sortedCorrectly") {
                    assertEquals("aaa", response!![0].name)
                    assertEquals("bbb", response!![1].name)
                    assertEquals("ccc", response!![2].name)
                    assertEquals("ddd", response!![3].name)
                }
            }

            context("Sort order is DESC") {
                val sortOrder = SortOrder.DESC
                var response: List<File>? = null

                beforeEachTest {
                    response = filesList.sortedWith(callMethod(sortField, sortOrder))
                }

                it("should be sortedCorrectly") {
                    assertEquals("ddd", response!![0].name)
                    assertEquals("ccc", response!![1].name)
                    assertEquals("bbb", response!![2].name)
                    assertEquals("aaa", response!![3].name)
                }
            }
        }

        context("Sort field is DESCRIPTION") {
            val sortField = SortField.DESCRIPTION

            context("Sort order is ASC") {
                val sortOrder = SortOrder.ASC
                var response: List<File>? = null

                beforeEachTest {
                    response = filesList.sortedWith(callMethod(sortField, sortOrder))
                }

                it("should be sortedCorrectly") {
                    assertEquals("aaa", response!![0].description)
                    assertEquals("bbb", response!![1].description)
                    assertEquals("ccc", response!![2].description)
                    assertEquals("ddd", response!![3].description)
                }
            }

            context("Sort order is DESC") {
                val sortOrder = SortOrder.DESC
                var response: List<File>? = null

                beforeEachTest {
                    response = filesList.sortedWith(callMethod(sortField, sortOrder))
                }

                it("should be sortedCorrectly") {
                    assertEquals("ddd", response!![0].description)
                    assertEquals("ccc", response!![1].description)
                    assertEquals("bbb", response!![2].description)
                    assertEquals("aaa", response!![3].description)
                }
            }
        }

        context("Sort field is DATE") {
            val sortField = SortField.DATE

            context("Sort order is ASC") {
                val sortOrder = SortOrder.ASC
                var response: List<File>? = null

                beforeEachTest {
                    response = filesList.sortedWith(callMethod(sortField, sortOrder))
                }

                it("should be sortedCorrectly") {
                    assertEquals(LocalDate.of(2019, 5, 5), response!![0].date)
                    assertEquals(LocalDate.of(2019, 5, 10), response!![1].date)
                    assertEquals(LocalDate.of(2019, 5, 15), response!![2].date)
                    assertEquals(LocalDate.of(2019, 5, 20), response!![3].date)
                }
            }

            context("Sort order is DESC") {
                val sortOrder = SortOrder.DESC
                var response: List<File>? = null

                beforeEachTest {
                    response = filesList.sortedWith(callMethod(sortField, sortOrder))
                }

                it("should be sortedCorrectly") {
                    assertEquals(LocalDate.of(2019, 5, 20), response!![0].date)
                    assertEquals(LocalDate.of(2019, 5, 15), response!![1].date)
                    assertEquals(LocalDate.of(2019, 5, 10), response!![2].date)
                    assertEquals(LocalDate.of(2019, 5, 5), response!![3].date)
                }
            }
        }
    }
})