package com.dedio.data.repository

import com.dedio.domain.model.files.File
import com.dedio.domain.model.files.FilesResponse
import com.dedio.domain.repository.FilesRepository
import io.reactivex.Observable
import org.threeten.bp.LocalDate
import javax.inject.Inject

class FilesRepositoryImpl @Inject constructor() : FilesRepository {

    override fun getFiles(): Observable<FilesResponse> {
        val files = ArrayList<File>()
        files.add(File("Sample", "This is a description",
                "https://s3-us-west-2.amazonaws.com/android-task/sample.pdf", LocalDate.of(2019, 5, 10)))
        files.add(File("Sample 2", "Description 2 is here",
                "https://s3-us-west-2.amazonaws.com/android-task/sample+2.pdf", LocalDate.of(2019, 5, 11)))
        files.add(File("Sample 3", "Description 3 is here",
                "https://s3-us-west-2.amazonaws.com/android-task/sample+3.pdf", LocalDate.of(2019, 5, 13)))
        files.add(File("Sample 4", "This is a unique description",
                "https://s3-us-west-2.amazonaws.com/android-task/sample+4.pdf", LocalDate.of(2019, 5, 15)))
        files.add(File("Sample 5", "This description is short",
                "https://s3-us-west-2.amazonaws.com/android-task/sample+5.pdf", LocalDate.of(2019, 5, 20)))

        val filesResponse = FilesResponse(files)

        return Observable.defer { Observable.just(filesResponse) }
    }
}