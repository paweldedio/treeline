package com.dedio.data.repository

import com.dedio.domain.net.PublicRestApi
import com.dedio.domain.repository.PdfRepository
import io.reactivex.Observable
import okhttp3.ResponseBody
import retrofit2.Response
import javax.inject.Inject

class PdfRepositoryImpl @Inject constructor(private val restApi: PublicRestApi) : PdfRepository {

    override fun getPdfFile(url: String): Observable<Response<ResponseBody>> {
        return restApi.loadPdf(url)
    }
}