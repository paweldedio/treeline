package com.dedio.data.util

import com.dedio.domain.model.files.File
import com.dedio.domain.model.files.SortField
import com.dedio.domain.model.files.SortOrder
import com.dedio.domain.util.FilesSortHelper
import javax.inject.Inject

class FilesSortHelperImpl @Inject constructor(): FilesSortHelper {

    override fun getFilesComparator(sortField: SortField, sortOrder: SortOrder): Comparator<File> {
        return when(sortField) {
            SortField.NAME -> getNameComparator(sortOrder)
            SortField.DESCRIPTION -> getDescriptionComparator(sortOrder)
            SortField.DATE -> getDateComparator(sortOrder)
        }
    }

    private fun getNameComparator(sortOrder: SortOrder): Comparator<File> {
        return if(shouldNegate(sortOrder)) {
            compareByDescending { it.name }
        } else {
            compareBy { it.name }
        }
    }

    private fun getDescriptionComparator(sortOrder: SortOrder):Comparator<File> {
        return if(shouldNegate(sortOrder)) {
            compareByDescending { it.description }
        } else {
            compareBy { it.description }
        }
    }

    private fun getDateComparator(sortOrder: SortOrder): Comparator<File> {
        return if(shouldNegate(sortOrder)) {
            compareByDescending { it.date }
        } else {
            compareBy { it.date }
        }
    }

    private fun shouldNegate(sortOrder: SortOrder): Boolean = sortOrder == SortOrder.DESC
}