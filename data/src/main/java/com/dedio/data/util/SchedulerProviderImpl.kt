package com.dedio.data.util

import com.dedio.domain.util.SchedulerProvider
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class SchedulerProviderImpl @Inject constructor(): SchedulerProvider {

    override fun provideIOScheduler(): Scheduler = Schedulers.io()

    override fun provideComputationScheduler(): Scheduler = Schedulers.computation()

    override fun provideMainThreadScheduler(): Scheduler = AndroidSchedulers.mainThread()
}