package com.dedio.data.net

import com.dedio.domain.net.PublicRestApi
import com.dedio.domain.net.PublicRestApiProvider
import com.dedio.domain.util.API_TIMEOUT
import com.google.gson.Gson

class PublicRestApiProviderImpl :
    BaseRetrofitApiProvider<PublicRestApi>(API_TIMEOUT, PublicRestApi::class.java),
    PublicRestApiProvider {

    override fun provideRestApi(): PublicRestApi = getApi()
}