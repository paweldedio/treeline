package com.dedio.data.net

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import java.util.concurrent.TimeUnit

open class BaseRetrofitApiProvider<A>(timeout: Long, apiClass: Class<A>) {

    private val api: A

    init {
        api = Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(buildClient(timeout))
                .baseUrl("http://template.url-wont.be.used.com")
                .build()
                .create(apiClass)
    }

    private fun buildClient(timeout: Long): OkHttpClient {
        val loggingInterceptor = HttpLoggingInterceptor()
        loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY

        val builder = OkHttpClient.Builder()
                .readTimeout(timeout, TimeUnit.SECONDS)
                .connectTimeout(timeout, TimeUnit.SECONDS)

        val networkInterceptor = createNetworkInterceptor()
        if (networkInterceptor != null) {
            builder.addNetworkInterceptor(networkInterceptor)
        }

        builder.addInterceptor(loggingInterceptor)

        return builder.build()
    }

    protected open fun createNetworkInterceptor(): Interceptor? = null

    fun getApi(): A = api
}